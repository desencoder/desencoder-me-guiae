import React from "react";
import { Text } from "react-native";

import ScreenContainer from "../../components/ScreenContainer";

const Tips = () => {
  return (
    <ScreenContainer>
      <Text>Tips</Text>
    </ScreenContainer>
  );
};

export default Tips;
